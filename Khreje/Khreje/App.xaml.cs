﻿using Khreje.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Khreje
{
    public partial class App : Application
    {
        public static string CurrentLanguage = "ar";
        public static string DeviceCalture = "ar-AE";
        public static FlowDirection Direction = FlowDirection.RightToLeft;
        public static bool flag = false;
        public App()
        {

            //AppResources.Culture = CrossMultilingual.Current.DeviceCultureInfo;
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MzM3MzhAMzEzNjJlMzMyZTMwQ29BeTd3SzhaTDZidGhRalQrTVE4MndSV1VHWFJSUXdTOXNOK2loWnhHTT0=");
            InitializeComponent();


            MainPage = new NavigationPage(new GenderPage())
            {
                BarBackgroundColor = Color.FromHex("#326b9f"),
                BarTextColor = Color.FromHex("#FFF")
            };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
