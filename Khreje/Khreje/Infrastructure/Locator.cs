﻿using Khreje.ViewModels;

namespace Khreje.Infrastructure
{
    public class Locator
    {
        public MainViewModel Main { get; set; }
        public Locator()
        {
            Main = new MainViewModel();
        }
    }
}
