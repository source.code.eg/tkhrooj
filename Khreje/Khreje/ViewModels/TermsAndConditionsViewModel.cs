﻿using Khreje.Models;
using Khreje.Services;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class TermsAndConditionsViewModel : BaseViewModel
    {
        private ApiService _apiService;

        private Setting _terms;

        public Setting Terms
        {
            get => _terms;
            set
            {
                SetValue(ref _terms, value);
            }
        }

        public TermsAndConditionsViewModel()
        {
            _apiService = new ApiService();
            LoadData();
        }

        private async void LoadData()
        {
            var url = Application.Current.Resources["BaseUrl"].ToString();

            var response = await _apiService.Get<Setting>(url,
                "api/Settings",
                "/GetPageSetting/",
                2);

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert("Error", response.Message, "Cancel");
                return;
            }
            Terms = response.Result as Setting;
        }
    }
}
