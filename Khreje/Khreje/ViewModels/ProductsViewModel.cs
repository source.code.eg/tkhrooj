﻿using Khreje.Models;
using Khreje.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class ProductsViewModel : BaseViewModel
    {
        private ObservableCollection<Product> _products;
        private ApiService _apiService;
        private int _id;
        public ObservableCollection<Product> Products
        {
            get => _products;
            set
            {
                SetValue(ref _products, value);
            }
        }


        public ProductsViewModel(int id)
        {
            _apiService = new ApiService();
            _id = id;
            LoadData();
        }


        private async void LoadData()
        {
            var url = Application.Current.Resources["BaseUrl"].ToString();

            var response = await _apiService.GetList<Product>(url,
                "api/Products",
                $"/GetAllProducts/{_id}");
            if (!response.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }
            var locator = MainViewModel.StaticLocator();
            locator.ProductsList = response.Result as List<Product>;
            Products = new ObservableCollection<Product>(locator.ProductsList);

        }


    }
}
