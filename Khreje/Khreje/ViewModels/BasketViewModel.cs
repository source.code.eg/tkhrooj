﻿using Khreje.Helpers;
using Khreje.Models;
using Khreje.Resources;
using Khreje.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class BasketViewModel : BaseViewModel
    {
        bool flag = App.flag;
        private ObservableCollection<OrderDetails> _orderDetails;
        private ApiService _apiService;
        readonly Database _database;
        public ObservableCollection<OrderDetails> OrderDetails
        {
            get => _orderDetails;
            set
            {
                SetValue(ref _orderDetails, value);
            }
        }

        public ICommand ConfirmOrderCommand
        {
            get =>
                new Command(async () =>
                {
                    if (!flag)
                    {
                        await Application.Current.MainPage.DisplayAlert(AppResources.ErrorMessageWhenLogin,
                            AppResources.ErrorMessageWhenLogin,
                            AppResources.Accept);
                        return;
                    }
                    var confirmOrder = new ConfirmOrder();
                    confirmOrder.Bookings = new List<ConfirmOrderBooking>();
                    confirmOrder.OrderDetails = new List<ConfrimOrderProductDetails>();

                    foreach (var item in OrderDetails)
                    {
                        confirmOrder.OrderDetails.Add(new ConfrimOrderProductDetails
                        {
                            Qty = item.Qty,
                            SizeId = item.SizeId,
                            StyleId = item.StyleId
                        });
                    }

                    var url = Application.Current.Resources["BaseUrl"].ToString();
                    var token = MainViewModel.StaticLocator().Token;
                    var response = await _apiService.Post<ConfirmOrder>(url,
                        "api/Orders",
                        "/AddOrder",
                        token.TokenType,
                        token.AccessToken,
                        confirmOrder
                        );

                    if (!response.IsSuccess)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error",
                            response.Message,
                            "Acccept");
                        return;
                    }
                    await Application.Current.MainPage.DisplayAlert("Khareej", "Thank you for your ordering we will contact you soon as possible", "Accept");

                    RemoveAllItems();

                });
        }

        public BasketViewModel()
        {
            _database = new Database("Khereje");
            _apiService = new ApiService();
            OrderDetails = new ObservableCollection<OrderDetails>();
            LoadData();
        }


        private async void LoadData()
        {
            OrderDetails.Clear();
            IEnumerable<OrderDetails> orders;
            try
            {
                orders = _database.GetItems<OrderDetails>();

            }
            catch
            {

                await Application.Current.MainPage.DisplayAlert("Error", "No item added to basket list.", "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }

            foreach (var order in orders)
            {
                OrderDetails.Add(order);
            }
        }

        public async void RemoveAllItems()
        {

            try
            {
                _database.DeleteAll<OrderDetails>();
                await Application.Current.MainPage.Navigation.PopAsync();
            }
            catch
            {
                return;
            }


        }
    }
}
