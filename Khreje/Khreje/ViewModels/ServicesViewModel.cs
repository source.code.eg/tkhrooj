﻿using Khreje.Models;
using Khreje.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class ServicesViewModel : BaseViewModel
    {
        private ObservableCollection<Product> _items;
        private ApiService _apiService;
        private int _id;
        public ObservableCollection<Product> Items
        {
            get => _items;
            set
            {
                SetValue(ref _items, value);
            }
        }


        public ServicesViewModel(int id)
        {
            _apiService = new ApiService();
            _id = id;
            LoadData();
        }


        private async void LoadData()
        {
            var url = Application.Current.Resources["BaseUrl"].ToString();

            var response = await _apiService.GetList<Product>(url,
                "api/ServiceItems",
                $"/GetAllItems/{_id}");
            if (!response.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }
            var locator = MainViewModel.StaticLocator();
            locator.ProductsList = response.Result as List<Product>;
            Items = new ObservableCollection<Product>(locator.ProductsList);

        }

    }
}
