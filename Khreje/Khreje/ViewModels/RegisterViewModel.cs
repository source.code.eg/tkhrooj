﻿using Khreje.Helpers;
using Khreje.Models;
using Khreje.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class RegisterViewModel : BaseViewModel
    {
        private string _userName;
        private string _password;
        private string _confirmPassword;
        private string _email;
        private ApiService _apiService;

        public string UserName
        {
            get => _userName;
            set
            {
                SetValue(ref _userName, value);
            }
        }


        public string Email
        {
            get => _email;
            set
            {
                SetValue(ref _email, value);
            }
        }
        private string _phoneNumber;

        public string PhoneNumber
        {
            get => _phoneNumber;
            set
            {
                SetValue(ref _phoneNumber, value);
            }
        }


        public string Password
        {
            get => _password;
            set
            {
                SetValue(ref _password, value);
            }
        }

        public string ConfirmPassword
        {
            get => _confirmPassword;
            set
            {
                SetValue(ref _confirmPassword, value);
            }
        }

        public ICommand RegisterCommand
        {
            get => new Command(Register);

        }

        public RegisterViewModel()
        {
            _apiService = new ApiService();
        }

        public async void Register()
        {
            if (string.IsNullOrEmpty(this.UserName))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Please enter your user name.",
                    "Accept");
                return;
            }



            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Please Enter your email",
                    "Accept");
                return;
            }

            if (!RegexUtilities.IsValidEmail(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Please enter a correct email.",
                    "Accept");
                return;
            }

            if (string.IsNullOrEmpty(this.PhoneNumber))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Please enter your phone number.",
                    "Accept");
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Please enter your password.",
                    "Accept");
                return;
            }

            if (this.Password.Length < 6)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Your password is less than 6 characters.",
                    "Accept");
                return;
            }

            if (string.IsNullOrEmpty(this.ConfirmPassword))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Please Enter a confirmation of password",
                    "Accept");
                return;
            }

            if (this.Password != this.ConfirmPassword)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "your password is not match with its confirmation.",
                    "Accept");
                return;
            }


            var checkConnetion = await this._apiService.CheckConnection();
            if (!checkConnetion.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    checkConnetion.Message,
                    "Accept");
                return;
            }

            var apiSecurity = Application.Current.Resources["BaseUrl"].ToString();
            var user = new User
            {
                UserName = UserName,
                Email = Email,
                Password = Password,
                PhoneNumber = PhoneNumber,
                ConfirmPassword = Password,
                Gender = true
            };
            var response = await this._apiService.Post(
                apiSecurity,
                "api",
                "/Account/Register",
                user);

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                return;
            }

            await Application.Current.MainPage.DisplayAlert(
                "Register is successful",
                "You have been registerd successfully.",
                "Accept");
            await Application.Current.MainPage.Navigation.PopAsync();

        }


    }
}
