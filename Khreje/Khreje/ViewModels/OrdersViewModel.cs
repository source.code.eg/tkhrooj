﻿using Khreje.Models;
using Khreje.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class OrdersViewModel
        : BaseViewModel
    {
        private ApiService _apiService;
        private ObservableCollection<Order> _orders;

        public ObservableCollection<Order> Orders
        {
            get => _orders;
            set
            {
                SetValue(ref _orders, value);
            }
        }

        public OrdersViewModel()
        {
            _apiService = new ApiService();
            LoadOrders();
        }

        private async void LoadOrders()
        {
            var url = Application.Current.Resources["BaseUrl"].ToString();
            var token = MainViewModel.StaticLocator().Token;
            var response = await _apiService.GetList<Order>(url,
                "api/Orders",
                "/GetMyOrders",
                token.TokenType, token.AccessToken);

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert("Error", response.Message, "Cancel");
                return;
            }
            var lstOrders = response.Result as List<Order>;
            Orders = new ObservableCollection<Order>(lstOrders);
        }
    }
}
