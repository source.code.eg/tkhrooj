﻿using Khreje.Models;
using Khreje.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class SuggestionsViewModel : BaseViewModel
    {
        private ApiService _apiService;

        private Suggestion _suggestion;



        private string _email;

        public string Email
        {
            get => _email;
            set
            {
                SetValue(ref _email, value);
            }
        }

        private string _userName;

        public string UserName
        {
            get => _userName;
            set
            {
                SetValue(ref _userName, value);
            }
        }

        private string _message;

        public string Message
        {
            get => _message;
            set
            {
                SetValue(ref _message, value);
            }
        }


        public SuggestionsViewModel()
        {
            _apiService = new ApiService();
        }
        public ICommand SendCommand
        {
            get => new Command(async () =>
            {
                var url = Application.Current.Resources["BaseUrl"].ToString();

                var token = MainViewModel.StaticLocator().Token;
                _suggestion = new Suggestion
                {
                    Message = Message,
                    Email = Email,
                    UserName = UserName
                };
                var response = await _apiService.Post<Suggestion>(url,
                    "api/TechnicalSupport",
                    "/SendProblem",
                    token.TokenType,
                    token.AccessToken,
                    _suggestion);

                if (!response.IsSuccess)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", "Please try again", "Accept");
                    return;
                }
                await Application.Current.MainPage.DisplayAlert("Suggestion", "Your suggestion has been sent successfuly.", "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();

            });

        }

    }
}
