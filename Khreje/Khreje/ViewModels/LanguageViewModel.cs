﻿using Khreje.Views;
using System.Windows.Input;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class LanguageViewModel : BaseViewModel
    {
        private string _SelectedLanguage;

        public ICommand TranslateToArabicCommand
        {
            get => new Command(() =>
            {
                _SelectedLanguage = "ar";
                SetLanguage();
            });
        }

        public ICommand TranslateToEnglishCommand
        {
            get => new Command(() =>
            {
                _SelectedLanguage = "en";
                SetLanguage();
            });
        }


        public LanguageViewModel()
        {

            _SelectedLanguage = App.CurrentLanguage;
        }

        private void SetLanguage()
        {
            if (_SelectedLanguage == "en")
            {
                App.Direction = FlowDirection.LeftToRight;
                App.DeviceCalture = "en-US";
            }
            else
            {
                App.Direction = FlowDirection.RightToLeft;
                App.DeviceCalture = "ar-AE";
            }
            App.CurrentLanguage = _SelectedLanguage;
            MainViewModel.StaticLocator().Gender = new GenderViewModel();
            Application.Current.MainPage = new NavigationPage(new GenderPage());
        }
    }
}
