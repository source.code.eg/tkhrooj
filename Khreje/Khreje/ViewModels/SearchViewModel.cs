﻿using Khreje.Models;
using Khreje.Services;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class SearchViewModel : BaseViewModel
    {
        private ObservableCollection<MainList> _galleryInfo;
        private ApiService _apiService;
        public ObservableCollection<MainList> GalleryInfo
        {
            get => _galleryInfo;
            set
            {
                SetValue(ref _galleryInfo, value);
            }
        }
        private string _filter;

        public string Filter
        {
            get => _filter;
            set
            {
                SetValue(ref _filter, value);
            }
        }

        public ICommand SearchCommand { get => new Command(LoadData); }


        public SearchViewModel()
        {
            _apiService = new ApiService();

        }

        private void LoadData()
        {
            var url = Application.Current.Resources["BaseUrl"].ToString();

            var response = _apiService
                .GetList<MainList>(url,
                "api/Search/",
                $"GetItems?content={Filter}");
        }
    }
}
