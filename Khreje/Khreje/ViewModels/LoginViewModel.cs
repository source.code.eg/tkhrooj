﻿using Khreje.Services;
using Khreje.Views;
using System.Windows.Input;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        #region BackingFields
        private string _email;
        private string _password;
        private ApiService _apiService;
        #endregion

        public string Email
        {
            get => _email;
            set
            {
                SetValue(ref _email, value);
            }
        }


        public string Password
        {
            get => _password;
            set
            {
                SetValue(ref _password, value);
            }
        }


        public ICommand LoginCommand
        {
            get => new Command(Login);
        }
        public ICommand RegisterNewAccountCommand
        {
            get => new Command(Register);
        }

        public LoginViewModel()
        {
            _apiService = new ApiService();
        }
        private void Register()
        {
            var locator = MainViewModel.StaticLocator();
            locator.RegisterVm = new RegisterViewModel();
            Application.Current.MainPage.Navigation.PushAsync(new RegisterPage());
        }
        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                   "Error",
                   "Please enter your email.",
                    "Accept");
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Please enter your password.",
                    "Accept");
                return;
            }
            var connection = await _apiService.CheckConnection();

            if (!connection.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Please check your connection.",
                    "Accept");
                return;
            }
            var url = Application.Current.Resources["BaseUrl"].ToString();
            var token = await this._apiService.GetToken(
                url,
                this.Email,
                this.Password);

            if (token == null)
            {

                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Something wrong please try again.",
                    "Accept");
                return;
            }

            if (string.IsNullOrEmpty(token.AccessToken))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    token.ErrorDescription,
                    "Accept");
                this.Password = string.Empty;
                return;
            }

            var staticLocator = MainViewModel.StaticLocator();

            staticLocator.Token = token;
            staticLocator.Home = new HomeViewModel();
            App.flag = true;
            await Application.Current.MainPage.Navigation.PushAsync(new HomePage());

        }
    }
}
