﻿using Khreje.Models;
using Khreje.Resources;
using Khreje.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class UpdateUserViewModel : BaseViewModel
    {
        private User _updateUser;
        private ApiService _apiService;

        public User UpdateUser
        {
            get => _updateUser;
            set
            {
                SetValue(ref _updateUser, value);
            }
        }
        public ICommand UpdateUserCommand
        {
            get => new Command(async () =>
            {
                UpdateUser.Email = "Sample@sample.com";
                UpdateUser.Password = "Pass.123";
                UpdateUser.ConfirmPassword = "Pass.123";
                var url = Application.Current.Resources["BaseUrl"].ToString();
                var token = MainViewModel.StaticLocator().Token;
                var response = await _apiService.Post<User>
                (url,
                "api/Account",
                "/UpdateUser",
                token.TokenType,
                token.AccessToken,
                UpdateUser);
                if (!response.IsSuccess)
                {
                    await Application.Current.MainPage.DisplayAlert(AppResources.ErrorTitle,
                    response.Message,
                    AppResources.Accept);
                    return;
                }

                await Application.Current.MainPage.DisplayAlert("Khareej", "You have been updated your information successfully.", "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
            });
        }
        public UpdateUserViewModel()
        {
            UpdateUser = new User();
            _apiService = new ApiService();

        }

    }
}
