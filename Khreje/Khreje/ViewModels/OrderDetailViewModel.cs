﻿using Khreje.Models;
using Khreje.Services;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class OrderDetailViewModel : BaseViewModel
    {
        private ApiService _apiService;
        private GetOrderDetail _getOrderDetail;
        private int _id;

        private Header _header;

        public Header Header
        {
            get => _header;
            set
            {
                SetValue(ref _header, value);
            }
        }

        private string _orderDate;

        public string OrderDate
        {
            get => _orderDate;
            set
            {
                SetValue(ref _orderDate, value);
            }
        }


        public GetOrderDetail GetOrderDetail
        {
            get => _getOrderDetail;
            set
            {
                SetValue(ref _getOrderDetail, value);
            }
        }
        public OrderDetailViewModel(int id, string orderDate)
        {
            _id = id;
            _apiService = new ApiService();
            _getOrderDetail = new GetOrderDetail();
            LoadOrderDetails();
        }

        private async void LoadOrderDetails()
        {
            var url = Application.Current.Resources["BaseUrl"].ToString();
            var token = MainViewModel.StaticLocator().Token;

            var response = await _apiService.Get<GetOrderDetail>(url,
                "api/Orders",
                $"?orderId={_id}", token.TokenType, token.AccessToken);

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    response.Message,
                    "Cancel");
                return;
            }
            GetOrderDetail = response.Result as GetOrderDetail;
            Header = new Header
            {
                OrderId = GetOrderDetail.OrderId,
                OrderDate = GetOrderDetail.OrderDate
            };
        }
    }
}
