﻿using Khreje.Models;
using Khreje.Services;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class AboutUsViewModel : BaseViewModel
    {
        private ApiService _apiService;

        private Setting _aboutUs;

        public Setting AboutUs
        {
            get => _aboutUs;
            set
            {
                SetValue(ref _aboutUs, value);
            }
        }


        public AboutUsViewModel()
        {
            _apiService = new ApiService();
            LoadData();
        }

        private async void LoadData()
        {
            var url = Application.Current.Resources["BaseUrl"].ToString();

            var response = await _apiService.Get<Setting>(url,
                "api/Settings/",
                "GetPageSetting/",
                1);

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert("Error", response.Message, "Cancel");
                return;
            }
            AboutUs = response.Result as Setting;
        }
    }
}
