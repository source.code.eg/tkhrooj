﻿using Khreje.Models;
using System.Collections.Generic;

namespace Khreje.ViewModels
{

    public class MainViewModel
    {
        private static MainViewModel _locator;
        // User Token.
        public TokenResponse Token
        {
            get;
            set;
        }

        // Main Lists.
        public List<MainList> CategoriesList { get; set; }

        public List<Product> ProductsList { get; set; }

        //public static ObservableCollection<OrderDetails> OrderDetails { get; set; }
        // Pages ViewModel.
        public CategoriesViewModel Category { get; set; }
        public HomeViewModel Home { get; set; }
        public GenderViewModel Gender { get; set; }
        public FavoritesViewModel Favorite { get; set; }
        public SuggestionsViewModel TechnicalVm { get; set; }
        public TermsAndConditionsViewModel TermsVm { get; set; }
        public LanguageViewModel LanguageVm { get; set; }
        public AboutUsViewModel AboutVm { get; set; }
        public BasketViewModel BasketVm { get; set; }
        public LoginViewModel LoginVm { get; set; }
        public ProductsViewModel ProductsVm { get; set; }
        public ProductDetailViewModel ProductDetailVm { get; set; }
        public OrdersViewModel OrderVm { get; set; }
        public RegisterViewModel RegisterVm { get; set; }
        public OrderDetailViewModel OrderDetailVm { get; set; }
        public SearchViewModel SearchVm { get; set; }
        public UpdateUserViewModel UpdateUserVm { get; set; }
        public ServicesViewModel ServicesVm { get; set; }
        public MainViewModel()
        {
            _locator = this;
            Gender = new GenderViewModel();
        }

        public static MainViewModel StaticLocator() => _locator;
    }
}
