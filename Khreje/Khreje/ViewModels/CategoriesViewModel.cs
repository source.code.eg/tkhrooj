﻿using Khreje.Models;
using System.Collections.ObjectModel;

namespace Khreje.ViewModels
{
    public class CategoriesViewModel : BaseViewModel
    {
        private ObservableCollection<CategoryList> _categories;

        public ObservableCollection<CategoryList> Categories
        {
            get => _categories;
            set
            {
                SetValue(ref _categories, value);
            }
        }
        public CategoriesViewModel()
        {
            LoadData();
        }
        private void LoadData()
        {
            Categories = new ObservableCollection<CategoryList>
            {
                new CategoryList
                {
                    Name = "Robes",
                    ImageName = "robe"
                },
                new CategoryList
                {
                    Name = "Caps",
                    ImageName = "cab"
                },
                new CategoryList
                {
                    Name = "Certificate",
                    ImageName = "certificate"
                },
                new CategoryList
                {
                    Name = "Graduation Caps",
                    ImageName = "gcaps"
                },
                new CategoryList
                {
                    Name = "Tags",
                    ImageName = "tags"
                },
                new CategoryList
                {
                    Name = "Certificates",
                    ImageName = "certApp"
                }
            };
        }

    }
}
