﻿using Khreje.Helpers;
using Khreje.Models;
using Khreje.Resources;
using Khreje.Services;
using Khreje.Views;
using Rg.Plugins.Popup.Services;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class ProductDetailViewModel : BaseViewModel
    {

        private Models.ProductDetail _productDetail;
        private int _id;
        private ApiService _apiServices;
        private int _qty = 1;
        private int _selectedSize = 1;
        bool flag = App.flag;
        private ObservableCollection<string> _sizes;


        public Models.ProductDetail ProductDetail

        {
            get => _productDetail;
            set
            {
                SetValue(ref _productDetail, value);
            }
        }

        private Models.Style _selectedStyle;

        public Models.Style SelectedStyle
        {
            get => _selectedStyle;
            set
            {
                SetValue(ref _selectedStyle, value);
            }
        }

        private double _rate;

        public double Rates
        {
            get => _rate;
            set
            {
                SetValue(ref _rate, value);
            }
        }


        public ObservableCollection<string> Sizes
        {
            get => _sizes;
            set
            {
                SetValue(ref _sizes, value);
            }
        }


        public int Qty
        {
            get => _qty;
            set
            {
                SetValue(ref _qty, value);
            }
        }


        private ObservableCollection<string> _rotator;

        public ObservableCollection<string> Rotator
        {
            get => _rotator;
            set
            {
                SetValue(ref _rotator, value);
            }
        }

        public int SelectedSize
        {
            get => _selectedSize;
            set
            {
                SetValue(ref _selectedSize, value);
            }
        }

        public ICommand BasketCommand
        {
            get => new Command(async () =>
            {
                if (SelectedStyle == null)
                {
                    await Application.Current.MainPage.DisplayAlert("Error",
                        "Please Select a style from styles list before submitting to cart.",
                        "Accept");
                    return;
                }
                var _database = new Database("Khereje");
                _database.CreateTable<OrderDetails>();
                _database.SaveItem<OrderDetails>(

                    new OrderDetails
                    {

                        OrderImage = "",
                        Name = ProductDetail.NameAR,
                        Qty = Qty,
                        SizeId = SelectedSize + 1,
                        StyleId = SelectedStyle.StyleId,
                        StyleName = SelectedStyle.StyleName

                    });
                await PopupNavigation.Instance.PushAsync(new AddedToCart());
            });
        }

        public ICommand AddToFavoriteCommand
        {
            get => new Command(async () =>
            {
                if (!flag)
                {
                    await Application.Current.MainPage.DisplayAlert(AppResources.ErrorMessageWhenLogin,
                        AppResources.ErrorMessageWhenLogin,
                        AppResources.Accept);
                    return;
                }
                var url = Application.Current.Resources["BaseUrl"].ToString();
                var token = MainViewModel.StaticLocator().Token;
                var response = await _apiServices.Post(url, "api/Favorites",
                    $"/AddProductToFavorites/{_id}",
                    token.TokenType,
                    token.AccessToken);
                if (!response.IsSuccess)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", response.Message, "Accept");
                    return;
                }
                await Application.Current.MainPage.DisplayAlert("Khereje",
                    "Your product has been added to successfuly",
                    "Accept");
            });
        }

        public ICommand GoToBasketCommand
        {
            get => new Command(async () =>
            {
                MainViewModel.StaticLocator().BasketVm = new BasketViewModel();
                await Application.Current.MainPage.Navigation.PushAsync(new BasketView());
            });
        }

        public ICommand AddRateCommand
        {
            get => new Command(async () =>
            {
                if (!flag)
                {
                    await Application.Current.MainPage.DisplayAlert(AppResources.ErrorTitle,
                        AppResources.ErrorMessageWhenLogin,
                        AppResources.Accept);
                    return;
                }
                await PopupNavigation.Instance.PushAsync(new AddRatePopup(ProductDetail.ProductId));
            });
        }


        public ProductDetailViewModel(int id, double rate)
        {
            Rates = rate;
            _id = id;
            _apiServices = new ApiService();
            LoadData();

        }

        private async void LoadData()
        {
            var url = Application.Current.Resources["BaseUrl"].ToString();


            var response = await _apiServices
                .Get<Models.ProductDetail>(url, "api/Products", "/GetProduct/", _id);
            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    response.Message,
                    "Accept");
                return;
            }
            Sizes = new ObservableCollection<string>
            {
                "S",
                "M",
                "L",
                "XL",
                "XXL",
                "XXXL"

            };


            ProductDetail = response.Result as Models.ProductDetail;
            Rotator = new ObservableCollection<string>
            {
                "robe",
                "robe",
                "robe"
            };

        }
    }
}
