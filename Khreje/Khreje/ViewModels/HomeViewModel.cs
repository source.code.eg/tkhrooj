﻿using Khreje.Models;
using Khreje.Resources;
using Khreje.Services;
using Khreje.Views;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        private string _filter;
        private ObservableCollection<MainList> _galleryInfo;
        private ObservableCollection<Models.Menu> _menuItems;
        private ObservableCollection<MainList> _categories;
        private ApiService _apiService;
        private bool IsArabic = App.CurrentLanguage == "ar" ? true : false;
        bool flag = App.flag;
        public ObservableCollection<MainList> GalleryInfo
        {
            get => _galleryInfo;
            set
            {
                SetValue(ref _galleryInfo, value);
            }
        }

        public string Filter
        {
            get => _filter;
            set
            {
                SetValue(ref _filter, value);
            }
        }

        public ObservableCollection<Models.Menu> MenuItems
        {
            get => _menuItems;
            set
            {
                SetValue(ref _menuItems, value);
            }
        }

        public ObservableCollection<MainList> Categories
        {
            get => _categories;
            set
            {
                SetValue(ref _categories, value);
            }
        }

        private ObservableCollection<Notification> _notifications;

        public ObservableCollection<Notification> Notifications
        {
            get => _notifications;
            set
            {
                SetValue(ref _notifications, value);
            }
        }

        public ICommand SearchCommand
        {
            get => new Command(LoadData);
        }

        public ICommand ExitCommand
        {
            get => new Command(async () =>
            {
                await Application.Current.MainPage.Navigation.PopAsync();
            });
        }

        public ICommand UpdateAccountCommand
        {
            get => new Command(UpdateAccount);
        }
        public ICommand FavoritesCommand
        {
            get => new Command(Favorites);
        }
        public ICommand BasketCommand { get => new Command(Basket); }
        public HomeViewModel()
        {
            _apiService = new ApiService();
            LoadMenu();
            LoadCategories();
            LoadNotifications();
        }
        private void LoadMenu()
        {
            MenuItems = new ObservableCollection<Models.Menu>
            {
                new Models.Menu
                {
                    Title = IsArabic? "تسجيل الدخول" : "Login",
                    Icon = "login",
                    PageName = "loginPage"
                },
                new Models.Menu
                {
                    Title = IsArabic? "اللغة" : "Language",
                    Icon = "language",
                    PageName = "languagePage"

                },
                new Models.Menu
                {
                    Title = IsArabic? "حول خريج" : "About Us",
                    Icon = "about",
                    PageName = "aboutPage"
                },
                new Models.Menu
                {
                    Title = IsArabic? "سلة المشتريات" : "Basket",
                    Icon = "basket",
                    PageName = "basketPage"
                },
                new Models.Menu
                {
                    Title = IsArabic? "الطلبات السابقة" : "Paid",
                    Icon = "paid",
                    PageName = "paidPage"
                },
                new Models.Menu
                {
                    Title = IsArabic? "الشروط والاحكام" : "Terms and Conditions",
                    Icon = "terms",
                    PageName="termsPage"
                },
                new Models.Menu
                {
                    Title = IsArabic? "الدعم الفني" : "Technical Support",
                    Icon = "tech",
                    PageName = "techPage"
                },
                new Models.Menu
                {
                    Title = IsArabic? "لديك مقترح" :  "Sugesstions?",
                    Icon = "help",
                    PageName = "suggestionsPage"
                },

            };
        }
        private async void LoadCategories()
        {
            var url = Application.Current.Resources["BaseUrl"].ToString();

            var response = await _apiService.GetList<MainList>(url,
                "api/Mainlist",
                "/GetMainList");
            if (!response.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }
            var locator = MainViewModel.StaticLocator();
            locator.CategoriesList = response.Result as List<MainList>;
            Categories = new ObservableCollection<MainList>(locator.CategoriesList);
        }

        private async void LoadNotifications()
        {
            var url = Application.Current.Resources["BaseUrl"].ToString();

            var response = await _apiService.GetList<Notification>(url,
                "api/Notifications",
                "/GetAllNotifications");

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    response.Message,
                    "Acccept");
                return;
            }
            var lst = response.Result as List<Notification>;
            Notifications = new ObservableCollection<Notification>(lst);

        }

        private async void UpdateAccount()
        {
            if (!flag)
            {
                await Application.Current.MainPage.DisplayAlert(AppResources.ErrorTitle,
                    AppResources.ErrorMessageWhenLogin,
                    AppResources.Accept);
                return;
            }
            MainViewModel.StaticLocator().UpdateUserVm = new UpdateUserViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new UpdateAccountPage());
        }
        private async void Favorites()
        {
            if (!flag)
            {
                await Application.Current.MainPage.DisplayAlert(AppResources.ErrorTitle,
                    AppResources.ErrorMessageWhenLogin,
                    AppResources.Accept);
                return;
            }
            MainViewModel.StaticLocator().Favorite = new FavoritesViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new FavoritesPage());

        }
        private async void Basket()
        {
            MainViewModel.StaticLocator().BasketVm = new BasketViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new BasketView());
        }
        private async void LoadData()
        {
            var url = Application.Current.Resources["BaseUrl"].ToString();

            var response = await _apiService
                .GetList<MainList>(url,
                "api/Search/",
                $"GetItems?content={Filter}");

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(AppResources.ErrorTitle, response.Message, AppResources.Exit);
                return;
            }
            var lst = response.Result as List<MainList>;
            GalleryInfo = new ObservableCollection<MainList>(lst);
        }

    }
}
