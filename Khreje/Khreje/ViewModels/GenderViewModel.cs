﻿using Khreje.Views;
using System.Windows.Input;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class GenderViewModel : BaseViewModel
    {
        public ICommand MaleCommand
        {
            get => new Command(MaleGender);
        }
        public ICommand FemeleCommand
        {
            get => new Command(FemaleGender);
        }
        private async void MaleGender()
        {
            Application.Current.Resources["IsLogin"] = false;
            MainViewModel.StaticLocator().Home = new HomeViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
        }
        private async void FemaleGender()
        {
            Application.Current.Resources["IsLogin"] = false;
            MainViewModel.StaticLocator().Home = new HomeViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
        }
    }
}
