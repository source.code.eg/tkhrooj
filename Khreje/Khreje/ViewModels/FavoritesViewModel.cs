﻿using Khreje.Models;
using Khreje.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace Khreje.ViewModels
{
    public class FavoritesViewModel : BaseViewModel
    {
        private ObservableCollection<Favorite> _favorites;

        private ApiService _apiService;
        public ObservableCollection<Favorite> Favorites
        {
            get => _favorites;
            set
            {
                SetValue(ref _favorites, value);
            }
        }

        public ICommand RemoveFavCommand
        {
            get => new Command<Favorite>(async (Favorite product) =>
            {

                var url = Application.Current.Resources["BaseUrl"].ToString();
                var token = MainViewModel.StaticLocator().Token;

                var response = await _apiService.Post(url,
                    "api/Favorites",
                    $"/RemoveProduct/{product.ProductId}",
                    token.TokenType,
                    token.AccessToken);
                if (!response.IsSuccess)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", response.Message, "Accept");
                    return;
                }
                LoadData();
            });
        }
        public FavoritesViewModel()
        {
            _apiService = new ApiService();
            LoadData();
        }
        private async void LoadData()
        {
            var url = Application.Current.Resources["BaseUrl"].ToString();
            var token = MainViewModel.StaticLocator().Token;
            var response = await _apiService.GetList<Favorite>(url, "api/Favorites/",
                "GetMyFavorites",
                token.TokenType, token.AccessToken);
            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    response.Message,
                    "Acccept");
                return;
            }
            var favLst = response.Result as List<Favorite>;
            Favorites = new ObservableCollection<Favorite>(favLst);
        }


    }
}
