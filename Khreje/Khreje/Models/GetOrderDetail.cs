﻿using System;
using System.Collections.Generic;

namespace Khreje.Models
{
    public class GetOrderDetail
    {
        public string Address { get; set; }
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public double DelivertCost { get; set; }
        public double ToTal { get; set; }
        public double TotalPrice { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }



    }
}
