﻿namespace Khreje.Models
{
    public class OrderDetails : BaseItem
    {
        public string Name { get; set; }
        public string OrderImage { get; set; }
        public int StyleId { get; set; }
        public string StyleName { get; set; }
        public int Qty { get; set; }
        public int SizeId { get; set; }
    }
}
