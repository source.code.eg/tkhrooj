﻿namespace Khreje.Models
{
    public class OrderDetail
    {
        public string NameAR { get; set; }
        public string NameEN { get; set; }
        public int Qty { get; set; }
        public string ImagePath { get; set; }
        public double Price { get; set; }

        public string ItemDetail { get => $"Qty {Qty}, Price{Price} KD"; }
        public string Name { get => App.CurrentLanguage == "ar" ? NameAR : NameEN; }
    }
}
