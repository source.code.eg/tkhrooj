﻿using System;

namespace Khreje.Models
{
    public class Header
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string BindingTime { get => OrderDate.ToShortDateString(); }
    }
}
