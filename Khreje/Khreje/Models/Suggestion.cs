﻿namespace Khreje.Models
{
    public class Suggestion
    {
        public string Message { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
