﻿namespace Khreje.Models
{
    public class CategoryList
    {
        public string Name { get; set; }
        public string ImageName { get; set; }
    }
}
