﻿using System.Collections.Generic;

namespace Khreje.Models
{
    public class ProductDetail
    {
        public int ProductId { get; set; }
        public string NameAR { get; set; }
        public string NameEN { get; set; }
        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }
        public bool Gender { get; set; }
        public string GenderType { get => Gender ? "Man" : "Woman"; }
        public List<string> Images { get; set; }
        public List<Style> Styles { get; set; }

        public string Name { get => App.CurrentLanguage == "ar" ? NameAR : NameEN; }
    }
}
