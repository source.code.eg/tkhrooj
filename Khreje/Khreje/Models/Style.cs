﻿namespace Khreje.Models
{
    public class Style
    {
        public int StyleId { get; set; }
        public string StyleName { get; set; }
    }
}
