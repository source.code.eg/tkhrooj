﻿namespace Khreje.Models
{
    public class Setting
    {
        public int SettingId { get; set; }
        public string HeaderAR { get; set; }
        public string HeaderEN { get; set; }
        public string ContentAR { get; set; }
        public string ContentEN { get; set; }

        public string Content { get => App.CurrentLanguage == "ar" ? ContentAR : ContentEN; }
    }
}
