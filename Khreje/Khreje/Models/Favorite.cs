﻿namespace Khreje.Models
{
    public class Favorite
    {
        public int ProductId { get; set; }
        public string NameAR { get; set; }
        public string NameEN { get; set; }
        public string Image { get; set; }
        public double Rates { get; set; }
        public int NoOfOrders { get; set; }

        public string Name { get => App.CurrentLanguage == "ar" ? NameAR : NameEN; }
    }
}
