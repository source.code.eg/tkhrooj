﻿using System;
using System.Collections.Generic;

namespace Khreje.Models
{
    public class ConfrimOrderProductDetails
    {
        public int StyleId { get; set; }
        public int Qty { get; set; }
        public int SizeId { get; set; }
        public string Color { get; set; }
    }
    public class ConfirmOrderBooking
    {
        public DateTime BookingDate { get; set; }
        public int Qty { get; set; }
        public int ItemServiceId { get; set; }
    }
    public class ConfirmOrder
    {
        public string Email { get; set; }
        public string Address { get; set; }
        public List<ConfrimOrderProductDetails> OrderDetails { get; set; }
        public List<ConfirmOrderBooking> Bookings { get; set; }
    }
}
