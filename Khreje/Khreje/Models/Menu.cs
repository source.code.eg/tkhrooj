﻿using Khreje.Resources;
using Khreje.ViewModels;
using Khreje.Views;
using Rg.Plugins.Popup.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace Khreje.Models
{
    public class Menu
    {
        public string Icon { get; set; }
        public string Title { get; set; }
        public string PageName { get; set; }
        public ICommand NavigateCommand { get => new Command(Navigate); }

        private async void Navigate()
        {
            var flag = App.flag;

            switch (PageName)
            {

                case "loginPage":
                    MainViewModel.StaticLocator().LoginVm = new LoginViewModel();
                    await Application.Current.MainPage.Navigation.PushAsync(new LoginPge());
                    break;
                case "languagePage":
                    MainViewModel.StaticLocator().LanguageVm = new LanguageViewModel();
                    await PopupNavigation.Instance.PushAsync(new LanguagePage());
                    break;

                case "aboutPage":
                    MainViewModel.StaticLocator().AboutVm = new AboutUsViewModel();
                    await Application.Current.MainPage.Navigation.PushAsync(new AboutUs());
                    break;

                case "termsPage":
                    MainViewModel.StaticLocator().TermsVm = new TermsAndConditionsViewModel();
                    await Application.Current.MainPage.Navigation.PushAsync(new TermsAndConditions());
                    break;

                case "basketPage":
                    MainViewModel.StaticLocator().BasketVm = new BasketViewModel();
                    await Application.Current.MainPage.Navigation.PushAsync(new BasketView());
                    break;
                case "paidPage":
                    if (!flag)
                    {
                        await Application.Current.MainPage.DisplayAlert(AppResources.ErrorTitle,
                            AppResources.ErrorMessageWhenLogin,
                            AppResources.Accept);
                        break;
                    }
                    MainViewModel.StaticLocator().OrderVm = new OrdersViewModel();
                    await Application.Current.MainPage.Navigation.PushAsync(new OrdersPage());
                    break;
                case "techPage":
                    MainViewModel.StaticLocator().TechnicalVm = new SuggestionsViewModel();
                    await Application.Current.MainPage.Navigation.PushAsync(new TechnicalSupportPage());
                    break;
                case "suggestionsPage":
                    MainViewModel.StaticLocator().TechnicalVm = new SuggestionsViewModel();
                    await Application.Current.MainPage.Navigation.PushAsync(new SuggestionsPage());
                    break;

            }
        }
    }
}
