﻿namespace Khreje.Models
{
    public class Category
    {
        private string _defaultImage;
        public int CategoryId { get; set; }
        public string NameAR { get; set; }
        public string NameEN { get; set; }
        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }

        public string Name { get => App.CurrentLanguage == "ar" ? NameAR : NameEN; }
        public string ImagePath
        {
            get => _defaultImage;
            set
            {
                _defaultImage = (value == null) ? "logo" : $"http://app.khareej-kw.com/Content/uploads/{value}";
            }
        }
        public object Products { get; set; }
    }
}
