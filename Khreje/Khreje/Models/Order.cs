﻿using System;
using System.Globalization;

namespace Khreje.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public DateTime PurchaseDate { get; set; }

        public string BindingTime { get => PurchaseDate.ToString("dd dddd MMMMM yyyy", new CultureInfo("ar-AE")); }
    }
}
