﻿using System;
using System.Globalization;

namespace Khreje.Models
{
    public class Notification
    {
        public int NotificationId { get; set; }
        public string Header { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }

        public string Content { get => Message + " " + Date.ToString("dd dddd MMMMM yyyy", new CultureInfo("ar-AE")); }
    }
}
