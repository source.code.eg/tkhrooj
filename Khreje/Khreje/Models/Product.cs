﻿namespace Khreje.Models
{
    public class Product
    {
        private string _defaultImage;

        public int ProductId { get; set; }
        public string NameAR { get; set; }
        public string NameEN { get; set; }
        public string ImagePath
        {
            get => _defaultImage;
            set
            {
                _defaultImage = (value == null) ? "logo" : $"http://app.khareej-kw.com/Content/uploads/{value}";
            }
        }
        public double Rates { get; set; }
        public int NoOfOrders { get; set; }

        public string Name { get => App.CurrentLanguage == "ar" ? NameAR : NameEN; }

    }
}
