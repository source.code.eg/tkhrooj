﻿namespace Khreje.Models
{
    public class MainList
    {
        private string _defaultImage;
        public int Id { get; set; }
        public string NameAR { get; set; }
        public string NameEN { get; set; }
        public string ImagePath
        {
            get => _defaultImage;
            set
            {
                _defaultImage = (value == null) ? "logo" : $"http://app.khareej-kw.com/Content/uploads/{value}";
            }
        }
        public bool IsProduct
        {
            get; set;
        }
        public string Name
        {
            get => App.CurrentLanguage == "ar" ? NameAR : NameEN;
        }
    }
}
