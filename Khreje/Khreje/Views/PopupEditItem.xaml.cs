﻿using Khreje.Helpers;
using Khreje.Models;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupEditItem : Rg.Plugins.Popup.Pages.PopupPage
    {
        readonly Database _database;

        OrderDetails _item;
        public PopupEditItem(OrderDetails item)
        {
            _item = item;
            _database = new Database("Khereje");
            InitializeComponent();
            FlowDirection = Xamarin.Forms.FlowDirection.LeftToRight;

            numericUpDown.Value = item.Qty;
            ObservableCollection<string> _sizes = new ObservableCollection<string>
            {
                "S",
                "M",
                "L",
                "XL",
                "XXL",
                "XXXL"
            };
            SizedList.ItemsSource = _sizes;
            SizedList.SelectedIndex = item.SizeId + 1;
        }

        private async void OnDeleteItem(object sender, System.EventArgs e)
        {
            _database.DeleteItem<OrderDetails>(_item.ID);
            await PopupNavigation.Instance.PopAsync();
        }

        private async void Button_Clicked_1(object sender, System.EventArgs e)
        {
            var newItem = new OrderDetails
            {
                Name = _item.Name,
                Qty = Convert.ToInt32(numericUpDown.Value),
                SizeId = SizedList.SelectedIndex + 1,
            };

            int id = _item.ID;
            _database.DeleteItem<OrderDetails>(id);
            _database.SaveItem<OrderDetails>(newItem);
            await PopupNavigation.Instance.PopAsync();

        }
    }
}