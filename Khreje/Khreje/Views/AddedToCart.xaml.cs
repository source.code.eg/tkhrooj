﻿using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddedToCart : Rg.Plugins.Popup.Pages.PopupPage
    {
        public AddedToCart()
        {
            FlowDirection = App.Direction;

            InitializeComponent();
        }
    }
}