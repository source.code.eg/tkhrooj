﻿
using Khreje.Services;
using Khreje.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductDetail : ContentPage
    {
        private ApiService _apiServices;
        public ProductDetail()
        {
            FlowDirection = App.Direction;

            _apiServices = new ApiService();
            InitializeComponent();
        }

        private async void ToolbarItem_Clicked(object sender, System.EventArgs e)
        {
            var _id = Convert.ToInt32(lblModel.Text);
            var url = Application.Current.Resources["BaseUrl"].ToString();
            var token = MainViewModel.StaticLocator().Token;
            var response = await _apiServices.Post<int>(url, "api/Favorites",
                "/AddProductToFavorites/",
                token.TokenType,
                token.AccessToken, 4);
            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert("Error", response.Message, "Accept");
                return;
            }
            await Application.Current.MainPage.DisplayAlert("Khereje",
                "Your product has been added to successfuly",
                "Accept");
        }
    }
}