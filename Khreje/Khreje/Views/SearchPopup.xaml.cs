﻿using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchPopup : Rg.Plugins.Popup.Pages.PopupPage
    {
        public SearchPopup()
        {
            InitializeComponent();
        }
    }
}