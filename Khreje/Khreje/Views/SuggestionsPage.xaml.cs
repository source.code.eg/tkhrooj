﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SuggestionsPage : ContentPage
    {
        public SuggestionsPage()
        {
            FlowDirection = App.Direction;

            InitializeComponent();
        }
    }
}