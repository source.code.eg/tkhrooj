﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GenderPage : ContentPage
    {
        public GenderPage()
        {
            FlowDirection = App.Direction;
            InitializeComponent();
        }
    }
}