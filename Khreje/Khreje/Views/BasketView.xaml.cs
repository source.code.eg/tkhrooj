﻿
using Khreje.Models;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BasketView : ContentPage
    {
        public BasketView()
        {
            FlowDirection = App.Direction;

            InitializeComponent();

        }

        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;
            var item = e.SelectedItem as OrderDetails;

            await PopupNavigation.Instance.PushAsync(new PopupEditItem(item));
            ((ListView)sender).SelectedItem = null;

        }
    }
}