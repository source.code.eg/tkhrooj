﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TechnicalSupportPage : ContentPage
    {
        public TechnicalSupportPage()
        {
            FlowDirection = App.Direction;

            InitializeComponent();
        }
    }
}