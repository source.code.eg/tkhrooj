﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            FlowDirection = App.Direction;

            InitializeComponent();
        }
    }
}