﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : TabbedPage
    {
        public HomePage()
        {
            FlowDirection = App.Direction;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }
    }
}