﻿using Khreje.Services;
using Khreje.ViewModels;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddRatePopup : Rg.Plugins.Popup.Pages.PopupPage
    {
        private ApiService _apiService = new ApiService();
        private int _productId;
        public AddRatePopup(int productId)
        {
            _productId = productId;
            InitializeComponent();
        }

        private async void Button_Clicked(object sender, System.EventArgs e)
        {
            var token = MainViewModel.StaticLocator().Token;
            var url = Application.Current.Resources["BaseUrl"].ToString();
            var response = await _apiService.Get(url,
                "api/Rates/", $"addRate?productId={_productId}&value={rating.Value}",
                token.TokenType, token.AccessToken);
            if (!response.IsSuccess)
            {
                await DisplayAlert("Error", response.Message, "Accept");
                return;
            }
            await PopupNavigation.Instance.PopAsync();

        }
    }
}