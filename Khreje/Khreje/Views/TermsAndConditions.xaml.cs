﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TermsAndConditions : ContentPage
    {
        public TermsAndConditions()
        {
            FlowDirection = App.Direction;

            InitializeComponent();
        }
    }
}