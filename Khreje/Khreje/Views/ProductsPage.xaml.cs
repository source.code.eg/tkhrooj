﻿
using Khreje.Models;
using Khreje.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductsPage : ContentPage
    {
        public ProductsPage()
        {
            FlowDirection = App.Direction;

            InitializeComponent();
        }

        private async void SfListView_ItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            var product = e.ItemData as Product;
            MainViewModel.StaticLocator().ProductDetailVm = new ProductDetailViewModel(product.ProductId, product.Rates);
            await Navigation.PushAsync(new ProductDetail());
            sflist.SelectionBackgroundColor = Color.Transparent;

        }
    }
}