﻿using Khreje.Models;
using Khreje.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CategoriesPage : ContentPage
    {
        public CategoriesPage()
        {
            FlowDirection = App.Direction;

            InitializeComponent();
        }

        private async void sflist_ItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            var category = e.ItemData as MainList;

            if (category.IsProduct)
            {
                MainViewModel.StaticLocator().ProductsVm = new ProductsViewModel(category.Id);
                await Navigation.PushAsync(new ProductsPage());
            }
            else
            {
                MainViewModel.StaticLocator().ServicesVm = new ServicesViewModel(category.Id);
                await Navigation.PushAsync(new ServicesPage());
            }

            sflist.SelectionBackgroundColor = Color.Transparent;
        }
    }
}