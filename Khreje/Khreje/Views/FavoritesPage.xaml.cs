﻿using Khreje.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FavoritesPage : ContentPage
    {
        private ApiService _apiServive = new ApiService();
        public FavoritesPage()
        {
            FlowDirection = App.Direction;

            InitializeComponent();
        }

        //private async void Button_Clicked(object sender, System.EventArgs e)
        //{

        //    var getSelectedItem = sender as MenuItem;
        //    var product = getSelectedItem.CommandParameter as Favorite;

        //    var url = Application.Current.Resources["BaseUrl"].ToString();
        //    var token = MainViewModel.StaticLocator().Token;

        //    await _apiServive.Post(url,
        //        "api/Favorites",
        //        $"RemoveProduct/{product.ProductId}",
        //        token.TokenType,
        //        token.AccessToken);

        //}
    }
}