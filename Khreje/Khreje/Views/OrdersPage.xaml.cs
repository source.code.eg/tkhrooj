﻿
using Khreje.Models;
using Khreje.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Khreje.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrdersPage : ContentPage
    {
        public OrdersPage()
        {
            FlowDirection = App.Direction;

            InitializeComponent();
        }

        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;
            var id = (e.SelectedItem as Order).OrderId;
            var dateValue = (e.SelectedItem as Order).BindingTime;
            MainViewModel.StaticLocator().OrderDetailVm = new OrderDetailViewModel(id, dateValue);
            await Navigation.PushAsync(new OrderDetailsPage());
            ((ListView)sender).SelectedItem = null;
        }
    }
}